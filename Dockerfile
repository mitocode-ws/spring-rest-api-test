FROM openjdk:11.0.6-jdk
LABEL usuario="Diani"
WORKDIR /workspace
COPY target/spring-rest-api-*.jar app.jar
EXPOSE 9090
ENTRYPOINT exec java -jar /workspace/app.jar
